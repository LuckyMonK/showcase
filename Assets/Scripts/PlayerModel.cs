﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerModel : MonoBehaviour
{
    private enum PlayerValues
    {
        Money
    }

    [SerializeField] private int _money;
    [SerializeField] private int money
    {
        get { return _money;}
        set
        {
            _money = value;
            moneyPanel.UpdateCoinValie(_money);
            PlayerPrefs.SetInt( PlayerValues.Money.ToString(), _money);
        }
    }
    public List<ListItem> items;

    [SerializeField] private MoneyPanel moneyPanel;


    private void Start()
    {
        RestoreBalance();
    }

    public bool TryBuyItem(ListItem target)
    {
        if (money >= target.price)
        {
            money -= target.price;
            items.Remove(target);
            return true;
        }
        else
        {
            return false;
        }

        
    }

    private void RestoreBalance()
    {
        money = PlayerPrefs.GetInt( PlayerValues.Money.ToString());
    }
}