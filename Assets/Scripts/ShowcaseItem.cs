﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class ShowcaseItem : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI currentNumber;
    [SerializeField] private TextMeshProUGUI cost;
    [SerializeField] private Transform contentRoot;
    [SerializeField] private Image iconPrefab;
    [SerializeField] private TextMeshProUGUI namePrefab;
    [SerializeField] private Button btn;
    
    public RectTransform rect;
    public Action onClickAction;

    private void Start()
    {
        btn.onClick.AddListener(() => { onClickAction.Invoke(); });
    }

    public void Fill(int numberInList, ListItem item, Action onBuyEvent, Action startValidation,
        PlayerModel playerModel)
    {
        currentNumber.text = numberInList.ToString();
        cost.text = item.price <= 0 ? "FREE" : item.price + " $";

        onClickAction += () =>
        {
            startValidation.Invoke();
            onBuyEvent.Invoke();
        };


        List<GameObject> content = item.GetContent(namePrefab, iconPrefab);

        foreach (var c in content)
        {
            c.transform.parent = contentRoot;
        }
    }
}