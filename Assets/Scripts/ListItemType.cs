﻿public enum ListItemType
{
    Image,
    Text,
    ImagePair,
    TextPair,
    ImageText,
    Currency
}