﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using  TMPro;

public class MoneyPanel : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI coinValue;

    public void UpdateCoinValie(int newValue)
    {
        coinValue.text = newValue.ToString();
    }
}
