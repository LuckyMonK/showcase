﻿using System;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Object = UnityEngine.Object;

[System.Serializable]
public class ListItem
{
    private static ListItemType[] KeyCodes = new ListItemType[]
    {
        ListItemType.Text,
        ListItemType.Image,
        ListItemType.ImageText,
        ListItemType.ImagePair,
        ListItemType.TextPair,
        ListItemType.Currency
    };
    
    [ValueDropdown("KeyCodes")]
    public ListItemType FilteredEnum;

    [ShowIf("FilteredEnum", ListItemType.Text)]
    public ListItemText text;
    
    [ShowIf("FilteredEnum", ListItemType.Image)]
    public ListItemIcon icon;

    [ShowIf("FilteredEnum", ListItemType.ImageText)]
    public ListItemIconText iconAndText;

    [ShowIf("FilteredEnum", ListItemType.ImagePair)]
    public ListItemIconPair doubleIcon;

    [ShowIf("FilteredEnum", ListItemType.TextPair)]
    public ListItemTextPair doubleText;

    public int price;


    private List<Dictionary<Object, Object>> content = new List<Dictionary<Object, Object>>();

    public List<GameObject> GetContent(TextMeshProUGUI prefabText, Image prefabImage)
    {
        switch (FilteredEnum)
        {
            case ListItemType.Text:
                return text.items(prefabText, prefabImage);
            case ListItemType.Image:
                return icon.items(prefabText, prefabImage);
            case ListItemType.ImageText:
                return iconAndText.items(prefabText, prefabImage);
            case ListItemType.ImagePair:
                return doubleIcon.items(prefabText, prefabImage);
            case ListItemType.TextPair:
                return doubleText.items(prefabText, prefabImage);
        }

        return new List<GameObject>();
    }

}

public interface IListItem
{
    List<GameObject> items(TextMeshProUGUI text, Image image);
}

[System.Serializable]
public class ListItemText : IListItem
{
    [SerializeField] public string Text;

    public List<GameObject> items(TextMeshProUGUI text, Image image)
    {
        List<GameObject> result = new List<GameObject>();
        var txt = GameObject.Instantiate(text);
        txt.text = Text;
        result.Add(txt.gameObject);

        return result;
    }
}

[System.Serializable]
public class ListItemIcon : IListItem
{
    [SerializeField] public Sprite Icon;
    public List<GameObject> items(TextMeshProUGUI text, Image image)
    {
        List<GameObject> result = new List<GameObject>();
        var txt = GameObject.Instantiate(image);
        image.sprite = Icon;
        result.Add(txt.gameObject);

        return result;
    }
}

[System.Serializable]
public class ListItemIconText : IListItem
{
    [SerializeField] public ListItemIcon Icon;
    [SerializeField] public ListItemText Text;
    public List<GameObject> items(TextMeshProUGUI text, Image image)
    {
        List<GameObject> result = new List<GameObject>();
        foreach (var i in Icon.items(text, image))
        {
            result.Add(i);
        }
        foreach (var i in Text.items(text, image))
        {
            result.Add(i);
        }

        return result;
    }
}

[System.Serializable]
public class ListItemIconPair: IListItem
{
    [SerializeField] public ListItemIcon Icon1;
    [SerializeField] public ListItemIcon Icon2;
    
    public List<GameObject> items(TextMeshProUGUI text, Image image)
    {
        List<GameObject> result = new List<GameObject>();
        foreach (var i in Icon1.items(text, image))
        {
            result.Add(i);
        }
        foreach (var i in Icon2.items(text, image))
        {
            result.Add(i);
        }

        return result;
    }
}
[System.Serializable]
public class ListItemTextPair: IListItem
{
    [SerializeField] public ListItemText Text1;
    [SerializeField] public ListItemText Text2;
    
    public List<GameObject> items(TextMeshProUGUI text, Image image)
    {
        List<GameObject> result = new List<GameObject>();
        foreach (var i in Text1.items(text, image))
        {
            result.Add(i);
        }
        foreach (var i in Text2.items(text, image))
        {
            result.Add(i);
        }
        return result;
    }
}