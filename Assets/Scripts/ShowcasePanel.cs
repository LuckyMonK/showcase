﻿using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.Utilities;
using UnityEngine;
using UnityEngine.UI;

public class ShowcasePanel : MonoBehaviour
{
    [SerializeField] private ShowcaseItem itemPrefab;
    [SerializeField] private PlayerModel player;
    [SerializeField] private RectTransform showcaseRoot;
    [SerializeField] private VerticalLayoutGroup verticalLayoutGroup;
    [SerializeField] private GameObject validationPanel;
    [SerializeField] private Button onValidationAcceptButton;
    [SerializeField] private Button onValidationDeclineButton;
    [SerializeField] private GameObject buyingFailWindow;
    [SerializeField] private Button buyingFailBtn;
    private ListItem currentItem;
    private void Start()
    {
        FillListView();
        onValidationAcceptButton.onClick.AddListener(() =>
        {
            if (player.TryBuyItem(currentItem))
            {
                FillListView();
                validationPanel.SetActive(false);
            }
            else
            {
                BuyingFail();
            }
        });
        
        onValidationDeclineButton.onClick.AddListener(() =>
        {
            validationPanel.SetActive(false);
        });
        
        buyingFailBtn.onClick.AddListener(() =>
        {
            buyingFailWindow.SetActive(false);
            validationPanel.SetActive(false);

        });
    }

    private void FillListView()
    {
        ClearShowcase();
        SetContentHeight();
        int counter = 1;
        foreach (var item in player.items)
        {
            FillCell(item, counter++);
        }
    }

    private void BuyingFail()
    {
        buyingFailWindow.SetActive(true);
    }

    private void FillCell(ListItem item, int counter)
    {
        var cell = Instantiate(itemPrefab, showcaseRoot);
        cell.Fill(counter, item, () =>
            {
                currentItem = item;
            }, 
            OpenValidation, player);
    }

    private void OpenValidation()
    {
        validationPanel.SetActive(true);
    }

    private void ClearShowcase()
    {
        foreach (Transform child in showcaseRoot)
        {
            Destroy(child.gameObject);
        }
    }

    private void SetContentHeight()
    {
        float newHeight =
            (itemPrefab.rect.rect.height + verticalLayoutGroup.spacing) *
            player.items.Count;
        showcaseRoot.sizeDelta = new Vector2(showcaseRoot.sizeDelta.x, newHeight);
    }
}